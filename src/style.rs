#[derive(Debug, Clone, Copy, PartialEq, Default)]
pub enum Color {
    #[default]
    Clear,
    Black,
    DarkGrey,
    Red,
    DarkRed,
    Green,
    DarkGreen,
    Yellow,
    DarkYellow,
    Blue,
    DarkBlue,
    Magenta,
    DarkMagenta,
    Cyan,
    DarkCyan,
    White,
    Grey,
    Rgb {
        r: u8,
        g: u8,
        b: u8,
    },
    AnsiValue(u8),
}

#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub enum Intensity {
    #[default]
    Normal,
    Bold,
    Dim,
}

#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub enum Underline {
    #[default]
    None,
    Single,
    Double,
    Curly,
    Dotted,
    Dashed,
}

#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub enum Blink {
    #[default]
    None,
    Slow,
    Fast,
}

#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub enum Italic {
    #[default]
    Normal,
    Italic,
}

#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub enum Reverse {
    #[default]
    Normal,
    Reverse,
}

#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub enum Strikethrough {
    #[default]
    Normal,
    Strikethrough,
}

#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub enum Visible {
    #[default]
    Visible,
    Invisible,
}

#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub enum Wrapped {
    #[default]
    None,
    Wrapped,
}

#[derive(Debug, Clone, Copy, Default, PartialEq)]
pub enum Overline {
    #[default]
    None,
    Overline,
}
