use crate::{Point, Rect};

pub trait Shape {
    type Outline;
    type Filled;

    fn outline(&self) -> Self::Outline;

    fn filled(&self) -> Self::Filled;
}

impl<T: Copy + num::Zero> Shape for Rect<T, T> {
    type Outline = RectOutline<T>;
    type Filled = RectFilled<T>;

    fn outline(&self) -> Self::Outline {
        RectOutline::new(self)
    }

    fn filled(&self) -> Self::Filled {
        RectFilled::new(self)
    }
}

pub struct RectFilled<T> {
    rect: Rect<T, T>,
    pos: Point<T>,
}

impl<T> RectFilled<T>
where
    T: Clone,
{
    pub fn new(rect: &Rect<T, T>) -> Self {
        RectFilled {
            rect: rect.clone(),
            pos: rect.pos.clone(),
        }
    }
}

impl<T> Iterator for RectFilled<T>
where
    T: num::One + std::ops::Add<Output = T> + std::ops::AddAssign + std::cmp::PartialOrd + Copy,
{
    type Item = Point<T>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.pos.y >= (self.rect.pos.y + self.rect.size.height) {
            return None;
        }

        let ret = self.pos;

        self.pos.x += num::one();
        if self.pos.x >= (self.rect.pos.x + self.rect.size.width) {
            self.pos.x = self.rect.pos.x;
            self.pos.y += num::one();
        }

        Some(ret)
    }
}

enum RectOutlineStep {
    Top,
    Left,
    Right,
    Bottom,
    Done,
}

pub struct RectOutline<T> {
    rect: Rect<T, T>,
    step: RectOutlineStep,
    position: T,
}

impl<T> RectOutline<T>
where
    T: num::Zero + Clone,
{
    pub fn new(rect: &Rect<T, T>) -> Self {
        RectOutline {
            rect: rect.clone(),
            step: RectOutlineStep::Top,
            position: num::zero(),
        }
    }
}

impl<T> Iterator for RectOutline<T>
where
    T: num::Zero
        + num::One
        + std::ops::AddAssign
        + std::ops::Add
        + std::ops::Sub<T, Output = T>
        + std::cmp::PartialOrd
        + Copy,
{
    type Item = Point<T>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.step {
            RectOutlineStep::Top => {
                let ret = Point {
                    x: self.position + self.rect.pos.x,
                    y: self.rect.pos.y,
                };

                self.position += num::One::one();

                if self.position >= self.rect.size.width {
                    self.position = num::One::one();
                    self.step = RectOutlineStep::Left;
                }

                Some(ret)
            }
            RectOutlineStep::Left => {
                let ret = Point {
                    x: self.rect.pos.x,
                    y: self.rect.pos.y + self.position,
                };

                self.position += num::One::one();

                if self.position >= (self.rect.size.height - num::One::one()) {
                    self.position = num::One::one();
                    self.step = RectOutlineStep::Right;
                }

                Some(ret)
            }
            RectOutlineStep::Right => {
                let ret = Point {
                    x: self.rect.pos.x + self.rect.size.width - num::One::one(),
                    y: self.rect.pos.y + self.position,
                };

                self.position += num::One::one();

                if self.position >= (self.rect.size.height - num::One::one()) {
                    self.position = num::zero();
                    self.step = RectOutlineStep::Bottom;
                }

                Some(ret)
            }
            RectOutlineStep::Bottom => {
                let ret = Point {
                    x: self.position + self.rect.pos.x,
                    y: self.rect.pos.y + self.rect.size.height - num::one(),
                };

                self.position += num::one();

                if self.position >= self.rect.size.width {
                    self.position = num::One::one();
                    self.step = RectOutlineStep::Done;
                }

                Some(ret)
            }
            RectOutlineStep::Done => None,
        }
    }
}
