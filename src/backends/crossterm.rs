use super::Backend;
use crate::events::{KeyCode, KeyEvent, KeyEventKind, Modifiers};
use crate::style::{
    Blink, Intensity, Italic, Overline, Reverse, Strikethrough, Underline, Visible, Wrapped,
};
use crate::{style::Color, Buffer, BufferView, Event, Point, Result, Size};
use std::io::Write;

pub struct Crossterm {
    stdout: std::io::Stdout,
    buffer: Buffer,
    back_buffer: Buffer,
}

impl Backend for Crossterm {
    fn new(title: String) -> Result<Self> {
        use crossterm::{event, terminal, QueueableCommand};
        let mut stdout = std::io::stdout();

        stdout
            .queue(terminal::DisableLineWrap)?
            .queue(terminal::EnterAlternateScreen)?
            .queue(event::EnableMouseCapture)?
            .queue(terminal::Clear(terminal::ClearType::All))?
            .queue(terminal::SetTitle(title))?;

        let _ = stdout.queue(event::PushKeyboardEnhancementFlags(
            event::KeyboardEnhancementFlags::DISAMBIGUATE_ESCAPE_CODES
                | event::KeyboardEnhancementFlags::REPORT_ALL_KEYS_AS_ESCAPE_CODES
                | event::KeyboardEnhancementFlags::REPORT_EVENT_TYPES,
        ));
        terminal::enable_raw_mode()?;

        let (width, height) = terminal::size()?;
        Ok(Crossterm {
            stdout,

            buffer: Buffer::new(width, height),
            back_buffer: Buffer::new(width, height),
        })
    }

    fn flush(&mut self) -> Result<()> {
        self.stdout.flush()?;
        Ok(())
    }

    fn set_cursor_pos(&mut self, cursor_pos: Option<Point<u16>>) -> Result<()> {
        use crossterm::{cursor, QueueableCommand};
        match cursor_pos {
            None => self.stdout.queue(cursor::Hide)?,
            Some(Point { x, y }) => self
                .stdout
                .queue(cursor::Show)?
                .queue(cursor::MoveTo(x, y))?,
        };

        Ok(())
    }

    fn clear(&mut self) -> Result<()> {
        use crossterm::{terminal, QueueableCommand};
        self.stdout
            .queue(terminal::Clear(terminal::ClearType::All))?;
        self.back_buffer.reset();
        Ok(())
    }

    fn resize(&mut self, width: u16, height: u16) -> Result<()> {
        self.buffer.resize(width, height);
        self.back_buffer.resize(width, height);
        self.clear()
    }

    fn draw(&mut self) -> Result<()> {
        use crossterm::{cursor, style, QueueableCommand};
        let diffs = self.buffer.get_diff(&self.back_buffer);

        let mut old_pos: Option<(u16, u16)> = None;
        let mut old_bg = Default::default();
        let mut old_fg = Default::default();
        let mut old_intensity = Default::default();
        let mut old_underline = Default::default();
        let mut old_blink = Default::default();
        let mut old_italic = Default::default();
        let mut old_strikethrough = Default::default();
        let mut old_reverse = Default::default();
        let mut old_visible = Default::default();
        let mut old_wrapped = Default::default();
        let mut old_overline = Default::default();

        for (pos, cell) in diffs {
            match (old_pos, pos) {
                (None, pos) => {
                    self.stdout.queue(cursor::MoveTo(pos.0, pos.1))?;
                }
                (Some(old_pos), pos) if pos != (old_pos.0 + 1, old_pos.1) => {
                    self.stdout.queue(cursor::MoveTo(pos.0, pos.1))?;
                }
                _ => {}
            }

            if cell.fg != old_fg {
                self.stdout
                    .queue(style::SetForegroundColor(cell.fg.into()))?;
            }
            if cell.bg != old_bg {
                self.stdout
                    .queue(style::SetBackgroundColor(cell.bg.into()))?;
            }
            if cell.intensity != old_intensity {
                self.stdout
                    .queue(style::SetAttribute(cell.intensity.into()))?;
            }
            if cell.underline != old_underline {
                self.stdout
                    .queue(style::SetAttribute(cell.underline.into()))?;
            }
            if cell.blink != old_blink {
                self.stdout.queue(style::SetAttribute(cell.blink.into()))?;
            }
            if cell.italic != old_italic {
                self.stdout.queue(style::SetAttribute(cell.italic.into()))?;
            }
            if cell.reverse != old_reverse {
                self.stdout
                    .queue(style::SetAttribute(cell.reverse.into()))?;
            }
            if cell.strikethrough != old_strikethrough {
                self.stdout
                    .queue(style::SetAttribute(cell.strikethrough.into()))?;
            }
            if cell.visible != old_visible {
                self.stdout
                    .queue(style::SetAttribute(cell.visible.into()))?;
            }
            if cell.wrapped != old_wrapped {
                self.stdout
                    .queue(style::SetAttribute(cell.wrapped.into()))?;
            }
            if cell.overline != old_overline {
                self.stdout
                    .queue(style::SetAttribute(cell.overline.into()))?;
            }

            self.stdout.queue(style::Print(&cell.c))?;

            old_pos = Some(pos);
            old_fg = cell.fg;
            old_bg = cell.bg;
            old_intensity = cell.intensity;
            old_underline = cell.underline;
            old_blink = cell.blink;
            old_italic = cell.italic;
            old_reverse = cell.reverse;
            old_strikethrough = cell.strikethrough;
            old_visible = cell.visible;
            old_wrapped = cell.wrapped;
            old_overline = cell.overline;
        }

        self.stdout
            .queue(style::ResetColor)?
            .queue(style::SetAttributes(style::Attributes::default()))?;
        Ok(())
    }

    fn swap_buffers(&mut self) -> Result<()> {
        std::mem::swap(&mut self.buffer, &mut self.back_buffer);
        self.buffer.reset();
        Ok(())
    }

    fn read_event(&mut self) -> Result<Option<Event>> {
        use crossterm::event;
        let event = event::read()?;
        let event = event.try_into().ok();
        Ok(event)
    }

    fn get_view(&mut self) -> BufferView {
        BufferView::from_buffer(&mut self.buffer)
    }
}

impl Crossterm {}

impl Drop for Crossterm {
    fn drop(&mut self) {
        use crossterm::{event, terminal, QueueableCommand};
        let _ = self.stdout.queue(event::PopKeyboardEnhancementFlags);
        let _ = self.stdout.queue(event::DisableMouseCapture);
        let _ = self.stdout.queue(terminal::LeaveAlternateScreen);
        let _ = self.stdout.queue(terminal::EnableLineWrap);

        let _ = self.stdout.flush();

        let _ = terminal::disable_raw_mode();
    }
}

impl From<crossterm::event::KeyEventKind> for KeyEventKind {
    fn from(value: crossterm::event::KeyEventKind) -> Self {
        match value {
            crossterm::event::KeyEventKind::Press => KeyEventKind::Press,
            crossterm::event::KeyEventKind::Repeat => KeyEventKind::Repeat,
            crossterm::event::KeyEventKind::Release => KeyEventKind::Release,
        }
    }
}
impl TryFrom<crossterm::event::Event> for Event {
    type Error = ();

    fn try_from(value: crossterm::event::Event) -> std::result::Result<Self, Self::Error> {
        use crossterm::event;
        match value {
            event::Event::FocusGained => Err(()),
            event::Event::FocusLost => Err(()),
            event::Event::Key(event::KeyEvent {
                code,
                modifiers,
                kind,
                ..
            }) => Ok(Event::Key(KeyEvent {
                key: code.try_into()?,
                modifiers: modifiers.try_into()?,
                kind: kind.into(),
            })),
            event::Event::Mouse(_) => Ok(Event::Mouse),
            event::Event::Paste(_) => Ok(Event::Paste),
            event::Event::Resize(width, height) => Ok(Event::Resized(Size { width, height })),
        }
    }
}

impl TryFrom<crossterm::event::KeyModifiers> for Modifiers {
    type Error = ();

    fn try_from(value: crossterm::event::KeyModifiers) -> std::result::Result<Self, Self::Error> {
        Self::from_bits(value.bits()).ok_or(())
    }
}

impl TryFrom<crossterm::event::KeyCode> for KeyCode {
    type Error = ();

    fn try_from(value: crossterm::event::KeyCode) -> std::result::Result<Self, Self::Error> {
        match value {
            crossterm::event::KeyCode::Backspace => Ok(KeyCode::Backspace),
            crossterm::event::KeyCode::Enter => Ok(KeyCode::Enter),
            crossterm::event::KeyCode::Left => Ok(KeyCode::Left),
            crossterm::event::KeyCode::Right => Ok(KeyCode::Right),
            crossterm::event::KeyCode::Up => Ok(KeyCode::Up),
            crossterm::event::KeyCode::Down => Ok(KeyCode::Down),
            crossterm::event::KeyCode::Home => Ok(KeyCode::Home),
            crossterm::event::KeyCode::End => Ok(KeyCode::End),
            crossterm::event::KeyCode::PageUp => Ok(KeyCode::PageUp),
            crossterm::event::KeyCode::PageDown => Ok(KeyCode::PageDown),
            crossterm::event::KeyCode::Tab => Ok(KeyCode::Tab),
            crossterm::event::KeyCode::BackTab => Ok(KeyCode::BackTab),
            crossterm::event::KeyCode::Delete => Ok(KeyCode::Delete),
            crossterm::event::KeyCode::Insert => Ok(KeyCode::Insert),
            crossterm::event::KeyCode::F(f) => Ok(KeyCode::F(f)),
            crossterm::event::KeyCode::Char(c) => Ok(KeyCode::Char(c)),
            crossterm::event::KeyCode::Null => Ok(KeyCode::Null),
            crossterm::event::KeyCode::Esc => Ok(KeyCode::Esc),
            crossterm::event::KeyCode::CapsLock => Ok(KeyCode::CapsLock),
            crossterm::event::KeyCode::ScrollLock => Ok(KeyCode::ScrollLock),
            crossterm::event::KeyCode::NumLock => Ok(KeyCode::NumLock),
            crossterm::event::KeyCode::PrintScreen => Ok(KeyCode::PrintScreen),
            crossterm::event::KeyCode::Pause => Ok(KeyCode::Pause),
            crossterm::event::KeyCode::Menu => Ok(KeyCode::Menu),
            crossterm::event::KeyCode::KeypadBegin => Ok(KeyCode::KeypadBegin),
            crossterm::event::KeyCode::Media(_) => todo!(), //Ok(KeyCode::Media(_)),
            crossterm::event::KeyCode::Modifier(_) => todo!(), // Ok(KeyCode::Modifier(_)),
        }
    }
}

impl From<Color> for crossterm::style::Color {
    fn from(value: Color) -> Self {
        match value {
            Color::Clear => crossterm::style::Color::Reset,
            Color::Black => crossterm::style::Color::Black,
            Color::DarkGrey => crossterm::style::Color::DarkGrey,
            Color::Red => crossterm::style::Color::Red,
            Color::DarkRed => crossterm::style::Color::DarkRed,
            Color::Green => crossterm::style::Color::Green,
            Color::DarkGreen => crossterm::style::Color::DarkGreen,
            Color::Yellow => crossterm::style::Color::Yellow,
            Color::DarkYellow => crossterm::style::Color::DarkYellow,
            Color::Blue => crossterm::style::Color::Blue,
            Color::DarkBlue => crossterm::style::Color::DarkBlue,
            Color::Magenta => crossterm::style::Color::Magenta,
            Color::DarkMagenta => crossterm::style::Color::DarkMagenta,
            Color::Cyan => crossterm::style::Color::Cyan,
            Color::DarkCyan => crossterm::style::Color::DarkCyan,
            Color::White => crossterm::style::Color::White,
            Color::Grey => crossterm::style::Color::Grey,
            Color::Rgb { r, g, b } => crossterm::style::Color::Rgb { r, g, b },
            Color::AnsiValue(v) => crossterm::style::Color::AnsiValue(v),
        }
    }
}

impl From<Intensity> for crossterm::style::Attribute {
    fn from(value: Intensity) -> Self {
        use crossterm::style::Attribute;
        match value {
            Intensity::Normal => Attribute::NormalIntensity,
            Intensity::Bold => Attribute::Bold,
            Intensity::Dim => Attribute::Dim,
        }
    }
}

impl From<Underline> for crossterm::style::Attribute {
    fn from(value: Underline) -> Self {
        use crossterm::style::Attribute;
        match value {
            Underline::None => Attribute::NoUnderline,
            Underline::Single => Attribute::Underlined,
            Underline::Double => Attribute::DoubleUnderlined,
            Underline::Curly => Attribute::Undercurled,
            Underline::Dotted => Attribute::Underdotted,
            Underline::Dashed => Attribute::Underdashed,
        }
    }
}

impl From<Blink> for crossterm::style::Attribute {
    fn from(value: Blink) -> Self {
        use crossterm::style::Attribute;
        match value {
            Blink::None => Attribute::NoBlink,
            Blink::Slow => Attribute::SlowBlink,
            Blink::Fast => Attribute::RapidBlink,
        }
    }
}

impl From<Italic> for crossterm::style::Attribute {
    fn from(value: Italic) -> Self {
        use crossterm::style::Attribute;
        match value {
            Italic::Normal => Attribute::NoItalic,
            Italic::Italic => Attribute::Italic,
        }
    }
}

impl From<Reverse> for crossterm::style::Attribute {
    fn from(value: Reverse) -> Self {
        use crossterm::style::Attribute;
        match value {
            Reverse::Normal => Attribute::NoReverse,
            Reverse::Reverse => Attribute::Reverse,
        }
    }
}

impl From<Strikethrough> for crossterm::style::Attribute {
    fn from(value: Strikethrough) -> Self {
        use crossterm::style::Attribute;
        match value {
            Strikethrough::Normal => Attribute::NotCrossedOut,
            Strikethrough::Strikethrough => Attribute::CrossedOut,
        }
    }
}

impl From<Visible> for crossterm::style::Attribute {
    fn from(value: Visible) -> Self {
        use crossterm::style::Attribute;
        match value {
            Visible::Invisible => Attribute::Hidden,
            Visible::Visible => Attribute::NoHidden,
        }
    }
}

impl From<Wrapped> for crossterm::style::Attribute {
    fn from(value: Wrapped) -> Self {
        use crossterm::style::Attribute;
        match value {
            Wrapped::None => Attribute::NotFramedOrEncircled,
            Wrapped::Wrapped => Attribute::Framed,
        }
    }
}

impl From<Overline> for crossterm::style::Attribute {
    fn from(value: Overline) -> Self {
        use crossterm::style::Attribute;
        match value {
            Overline::None => Attribute::NotOverLined,
            Overline::Overline => Attribute::OverLined,
        }
    }
}
