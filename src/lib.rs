pub mod backends;
pub mod events;
pub mod shapes;
pub mod style;

pub mod prelude {
    pub use crate::{
        backends::crossterm::Crossterm, events::Event, shapes::Shape, style::Color, Application,
        BufferView, Message, Point, Rect, Size,
    };
}

use events::Event;
use style::{
    Blink, Color, Intensity, Italic, Overline, Reverse, Strikethrough, Underline, Visible, Wrapped,
};

type Result<T> = std::result::Result<T, std::io::Error>;

pub fn run_in_runtime<B: backends::Backend>(app: impl Application) -> Result<()> {
    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap()
        .block_on(async { run::<B>(app) })
}

pub fn run<B: backends::Backend>(mut app: impl Application) -> Result<()> {
    let mut b = B::new(app.title())?;

    let mut view = b.get_view();
    let cursor_pos = app.view(&mut view);

    b.draw()?;
    b.swap_buffers()?;

    b.set_cursor_pos(cursor_pos)?;
    b.flush()?;
    loop {
        let mut redraw = false;

        let event = b.read_event()?;
        if let Some(Event::Resized(Size { width, height })) = event {
            b.resize(width, height)?;
            redraw = true;
        }

        if let Some(event) = event {
            match app.update(&event) {
                Some(Message::Redraw) => {
                    redraw = true;
                }
                Some(Message::Close) => {
                    break;
                }
                None => {}
            }
        }

        if redraw {
            let mut view = b.get_view();
            let cursor_pos = app.view(&mut view);

            b.draw()?;
            b.swap_buffers()?;
            b.set_cursor_pos(cursor_pos)?;
            b.flush()?;
        }
    }

    Ok(())
}

pub enum Message {
    Redraw,
    Close,
}

pub struct Buffer {
    data: Vec<Cell>,

    width: u16,
    height: u16,
}

impl Buffer {
    pub fn new(width: u16, height: u16) -> Self {
        Self {
            width,
            height,
            data: vec![Cell::default(); (width * height).into()],
        }
    }

    fn get_mut(&mut self, x: u16, y: u16) -> Option<&mut Cell> {
        if x >= self.width || y >= self.height {
            return None;
        }
        let index = self.get_index(x, y);
        self.data.get_mut(index)
    }

    fn get_index(&self, x: u16, y: u16) -> usize {
        self.width as usize * y as usize + x as usize
    }

    fn reset(&mut self) {
        for cell in self.data.iter_mut() {
            cell.reset();
        }
    }

    fn resize(&mut self, width: u16, height: u16) {
        let length = width as usize * height as usize;
        if self.data.len() > length {
            self.data.truncate(length);
        } else {
            self.data.resize(length, Default::default());
        }
        self.width = width;
        self.height = height;
    }

    fn get_diff(&self, other: &Buffer) -> Vec<((u16, u16), Cell)> {
        let back_buffer = other.data.iter();

        let zip = self
            .data
            .iter()
            .zip(back_buffer)
            .enumerate()
            .filter_map(|(index, (c1, c2))| c1.diff(c2).map(|d| (self.get_pos(index), d)));

        zip.collect()
    }

    fn get_pos(&self, index: usize) -> (u16, u16) {
        debug_assert!(
            index < self.data.len(),
            "Trying to get the coords of a cell outside the buffer: i={} len={}",
            index,
            self.data.len()
        );
        (index as u16 % self.width, index as u16 / self.width)
    }
}

pub struct BufferView<'a> {
    buffer: &'a mut Buffer,
    rect: Rect<i16, u16>,
}

impl<'a> BufferView<'a> {
    fn from_buffer(buffer: &'a mut Buffer) -> Self {
        let rect = Rect {
            pos: Point::default(),
            size: Size {
                width: buffer.width,
                height: buffer.height,
            },
        };
        Self { buffer, rect }
    }

    pub fn get_mut(&mut self, x: u16, y: u16) -> Option<&mut Cell> {
        if x >= self.rect.size.width || y >= self.rect.size.height {
            return None;
        }

        let x = self.rect.pos.x + x as i16;
        let y = self.rect.pos.y + y as i16;

        let Ok(x) = x.try_into() else {
            return None;
        };

        let Ok(y) = y.try_into() else {
            return None;
        };

        self.buffer.get_mut(x, y)
    }

    pub fn set_char(&mut self, x: u16, y: u16, c: char) {
        let _ = self.get_mut(x, y).map(|ch| {
            ch.set_char(c);
        });
    }

    pub fn pos(&self) -> Point<i16> {
        self.rect.pos
    }

    pub fn width(&self) -> u16 {
        self.rect.size.width
    }

    pub fn height(&self) -> u16 {
        self.rect.size.height
    }

    pub fn update(&mut self, cells: &mut impl Iterator<Item = Point<u16>>, f: impl Fn(&mut Cell)) {
        for Point { x, y } in cells {
            let Some(cell)  =           self.get_mut(x,y )else {
                continue;
            };

            f(cell);
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Cell {
    c: char,
    fg: Color,
    bg: Color,
    intensity: Intensity,
    underline: Underline,
    blink: Blink,
    italic: Italic,
    reverse: Reverse,
    strikethrough: Strikethrough,
    visible: Visible,
    wrapped: Wrapped,
    overline: Overline,
}

impl Default for Cell {
    fn default() -> Self {
        Self {
            c: ' ',
            fg: Default::default(),
            bg: Default::default(),
            intensity: Default::default(),
            underline: Default::default(),
            blink: Default::default(),
            italic: Default::default(),
            reverse: Default::default(),
            strikethrough: Default::default(),
            visible: Default::default(),
            wrapped: Default::default(),
            overline: Default::default(),
        }
    }
}

impl Cell {
    pub fn set_char(&mut self, c: char) -> &mut Self {
        self.c = c;
        self
    }

    pub fn set_foreground(&mut self, fg: Color) -> &mut Self {
        self.fg = fg;
        self
    }

    pub fn set_backround(&mut self, bg: Color) -> &mut Self {
        self.bg = bg;
        self
    }

    pub fn set_intensity(&mut self, intensity: Intensity) -> &mut Self {
        self.intensity = intensity;
        self
    }

    pub fn set_underline(&mut self, underline: Underline) -> &mut Self {
        self.underline = underline;
        self
    }

    pub fn set_blink(&mut self, blink: Blink) -> &mut Self {
        self.blink = blink;
        self
    }

    pub fn set_italic(&mut self, italic: Italic) -> &mut Self {
        self.italic = italic;
        self
    }

    pub fn set_reverse(&mut self, reverse: Reverse) -> &mut Self {
        self.reverse = reverse;
        self
    }

    pub fn set_strikethrough(&mut self, strikethrough: Strikethrough) -> &mut Self {
        self.strikethrough = strikethrough;
        self
    }

    pub fn set_visible(&mut self, visible: Visible) -> &mut Self {
        self.visible = visible;
        self
    }

    pub fn set_wrapped(&mut self, wrapped: Wrapped) -> &mut Self {
        self.wrapped = wrapped;
        self
    }

    pub fn set_overline(&mut self, overline: Overline) -> &mut Self {
        self.overline = overline;
        self
    }

    pub fn reset(&mut self) {
        self.c = ' ';
        self.fg = Default::default();
        self.bg = Default::default();
        self.intensity = Default::default();
        self.underline = Default::default();
        self.blink = Default::default();
        self.italic = Default::default();
        self.reverse = Default::default();
        self.strikethrough = Default::default();
        self.visible = Default::default();
        self.wrapped = Default::default();
        self.overline = Default::default();
    }

    fn diff(&self, other: &Self) -> Option<Self> {
        if self != other {
            Some(self.clone())
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, Copy, Default)]
pub struct Point<T> {
    pub x: T,
    pub y: T,
}

#[derive(Debug, Clone, Copy)]
pub struct Size<T> {
    pub width: T,
    pub height: T,
}

#[derive(Debug, Clone, Copy)]
pub struct Rect<TPos, TSize> {
    pub pos: Point<TPos>,
    pub size: Size<TSize>,
}

pub trait Application {
    fn title(&self) -> String;

    fn update(&mut self, event: &Event) -> Option<Message>;

    fn view(&self, buffer: &mut BufferView) -> Option<Point<u16>>;
}

pub trait Widget {
    fn update(&mut self, event: &Event) -> Option<Message>;

    fn view(&self, buffer: &mut BufferView) -> Option<Point<u16>>;
}
