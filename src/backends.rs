pub mod crossterm;

use crate::{BufferView, Event, Point, Result};

pub trait Backend: Sized {
    fn new(title: String) -> Result<Self>;
    fn read_event(&mut self) -> Result<Option<Event>>;
    fn flush(&mut self) -> Result<()>;
    fn set_cursor_pos(&mut self, cursor_pos: Option<Point<u16>>) -> Result<()>;
    fn clear(&mut self) -> Result<()>;
    fn resize(&mut self, width: u16, height: u16) -> Result<()>;
    fn draw(&mut self) -> Result<()>;
    fn swap_buffers(&mut self) -> Result<()>;

    fn get_view(&mut self) -> BufferView;
}
