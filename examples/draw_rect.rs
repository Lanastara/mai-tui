use mai_tui::{
    events::{KeyCode, KeyEvent, KeyEventKind},
    prelude::*,
    run_in_runtime,
    shapes::Shape,
    style::{Color, Overline},
    Application, BufferView, Message, Point, Rect, Size,
};

fn main() -> Result<(), std::io::Error> {
    run_in_runtime::<Crossterm>(Rectangle { draw_inner: false })?;

    Ok(())
}

struct Rectangle {
    draw_inner: bool,
}

impl Application for Rectangle {
    fn title(&self) -> String {
        "Draw Rectangle".to_string()
    }

    fn update(&mut self, event: &Event) -> Option<Message> {
        match event {
            Event::Key(KeyEvent {
                key: KeyCode::Char('q'),
                kind: KeyEventKind::Press,
                ..
            }) => Some(Message::Close),
            Event::Key(KeyEvent {
                key: KeyCode::Char('t'),
                kind: KeyEventKind::Press,
                ..
            }) => {
                self.draw_inner = !self.draw_inner;
                Some(Message::Redraw)
            }
            _ => None,
        }
    }

    fn view(&self, buffer: &mut BufferView) -> Option<Point<u16>> {
        let rect = Rect {
            pos: Point::default(),
            size: Size {
                width: buffer.width(),
                height: buffer.height(),
            },
        };

        let mut outline = rect.outline();

        buffer.update(&mut outline, |c| {
            c.set_char('#')
                .set_backround(Color::Red)
                .set_italic(mai_tui::style::Italic::Italic)
                // .set_reverse(mai_tui::style::Reverse::Reverse)
                .set_strikethrough(mai_tui::style::Strikethrough::Strikethrough)
                // .set_visible(mai_tui::style::Visible::Invisible)
                .set_overline(Overline::Overline);
        });

        let rect = Rect {
            pos: Point { y: 1, x: 1 },
            size: Size {
                width: 7,
                height: 7,
            },
        };

        let mut filled = rect.outline();
        buffer.update(&mut filled, |c| {
            c.set_char('#')
                .set_foreground(Color::Green)
                .set_underline(mai_tui::style::Underline::Double)
                .set_blink(mai_tui::style::Blink::Slow)
                .set_intensity(mai_tui::style::Intensity::Dim);
        });

        if self.draw_inner {}

        None
    }
}
